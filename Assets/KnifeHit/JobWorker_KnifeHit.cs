﻿using UnityEngine;
using System;

public class JobWorker_KnifeHit : MonoBehaviour
{
    public Action<string> onEnterScene;
    public Action onLink2Store;
    public Action onDailyGiftReceived;
    public Action onShowBanner;
    public Action onCloseBanner;
    public Action onShowFixedBanner;
    public Action onShowInterstitial;

    public static JobWorker_KnifeHit instance;

    private void Awake()
    {
        instance = this;
    }
}