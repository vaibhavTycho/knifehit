﻿using UnityEngine;

public class GameMaster_KnifeHit : MonoBehaviour{
    public static GameMaster_KnifeHit instance;
    private void Awake()
    {
        if (instance == null) instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }
}
