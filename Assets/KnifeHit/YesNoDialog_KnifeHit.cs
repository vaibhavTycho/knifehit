﻿using UnityEngine;
using System.Collections;
using System;

public class YesNoDialog_KnifeHit : Dialog_KnifeHit
{
    public Action onYesClick;
    public Action onNoClick;
    public virtual void OnYesClick()
    {
        if (onYesClick != null) onYesClick();
        Sound_KnifeHit.instance.PlayButton();
        Close();
    }

    public virtual void OnNoClick()
    {
        if (onNoClick != null) onNoClick();
        Sound_KnifeHit.instance.PlayButton();
        Close();
    }
}
