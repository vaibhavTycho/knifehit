﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//Paul Cornel//
public class KnifeShop_KnifeHit : MonoBehaviour {
	public GameObject shopUIParent;
	public ShopItem_KnifeHit shopKnifePrefab;
	public Transform shopPageContent;
	public Text unlockKnifeCounterLbl;
	public Button unlockNowBtn,unlockRandomBtn;
	public Image selectedKnifeImageUnlock;
	public Image selectedKnifeImageLock;
	public GameObject knifeBackeffect1,knifeBackeffect2;
	public int UnlockPrice=250,UnlockRandomPrice=250;
	public List<Knife_KnifeHit> shopKnifeList;

	public static KnifeShop_KnifeHit intance;
	public static ShopItem_KnifeHit selectedItem;
	public AudioClip onUnlocksfx,RandomUnlockSfx;
	List<ShopItem_KnifeHit> shopItems;
	ShopItem_KnifeHit selectedShopItem
	{
		get
		{ 
			return shopItems.Find ((obj) => { return obj.selected; });
		}
	}
	void Start() 
	{
		if (intance == null) 
		{
			intance = this;
			SetupShop ();
		}
	}
	[ContextMenu("Clear PlayerPref")]
	void ClearPlayerPrefs()
	{
		PlayerPrefs.DeleteAll ();
	}

	[ContextMenu("Add Apple")]
		void addApple()
		{
		GameManager_KnifeHit.Apple += 500;
		}
	public void showShop()
	{
		shopUIParent.SetActive (true);
		if (!shopItems [GameManager_KnifeHit.SelectedKnifeIndex].selected) {
			shopItems [GameManager_KnifeHit.SelectedKnifeIndex].selected = true;
		}
		UpdateUI ();

		CUtils_KnifeHit.ShowInterstitialAd();
	}
	void SetupShop ()
	{	
		unlockNowBtn.GetComponentInChildren<Text> ().text = UnlockPrice + "";
		unlockRandomBtn.GetComponentInChildren<Text> ().text = UnlockRandomPrice + "";

		shopItems = new List<ShopItem_KnifeHit> ();
		for (int i = 0; i < shopKnifeList.Count; i++) {
			ShopItem_KnifeHit temp = Instantiate<ShopItem_KnifeHit> (shopKnifePrefab, shopPageContent);
			temp.setup (i, this);
			temp.name = i + "";
			shopItems.Add (temp);
		}

		shopItems [GameManager_KnifeHit.SelectedKnifeIndex].OnClick ();
	}
	public void UpdateUI()
	{
        selectedKnifeImageUnlock.sprite = selectedShopItem.knifeImage.sprite;
        selectedKnifeImageLock.sprite = selectedShopItem.knifeImage.sprite;
        selectedKnifeImageUnlock.gameObject.SetActive(selectedShopItem.KnifeUnlock);
        selectedKnifeImageLock.gameObject.SetActive(!selectedShopItem.KnifeUnlock);

        knifeBackeffect1.SetActive(selectedShopItem.KnifeUnlock);
        knifeBackeffect2.SetActive(selectedShopItem.KnifeUnlock);

        int unlockCount = 0;
        if (shopItems.FindAll((obj) => { return obj.KnifeUnlock; }) != null)
        {
            unlockCount = shopItems.FindAll((obj) =>
            {
                return obj.KnifeUnlock;
            }).Count;
        }
        unlockKnifeCounterLbl.text = unlockCount + "/" + shopKnifeList.Count;
        if (unlockCount == shopKnifeList.Count)
        {
            unlockNowBtn.interactable = false;
            unlockRandomBtn.interactable = false;
        }

        GameManager_KnifeHit.selectedKnifePrefab = shopKnifeList[GameManager_KnifeHit.SelectedKnifeIndex];
        if (MainMenu_KnifeHit.intance != null)
        {
            MainMenu_KnifeHit.intance.selectedKnifeImage.sprite = GameManager_KnifeHit.selectedKnifePrefab.GetComponent<SpriteRenderer>().sprite;
        }
    }
	public void UnlockKnife()
	{
		if (unlockingRandom)
			return;
		
		if (GameManager_KnifeHit.Apple < UnlockPrice) 
		{
			Toast_KnifeHit.instance.ShowMessage("Opps! Don't have enough apples");
			SoundManager_KnifeHit.instance.PlaybtnSfx ();
			return;
		}
		if (selectedShopItem.KnifeUnlock) 
		{
			Toast_KnifeHit.instance.ShowMessage("It's already unlocked!");
			SoundManager_KnifeHit.instance.PlaybtnSfx ();
			return;
		}
		GameManager_KnifeHit.Apple -= UnlockPrice;
		selectedShopItem.KnifeUnlock = true;
		selectedShopItem.UpdateUIColor ();
		GameManager_KnifeHit.SelectedKnifeIndex = selectedShopItem.index;
		UpdateUI ();
		SoundManager_KnifeHit.instance.PlaySingle (onUnlocksfx);

	}
	bool unlockingRandom=false;
	public void UnlockRandomKnife()
	{
		if (GameManager_KnifeHit.Apple < UnlockRandomPrice) 
		{
			Toast_KnifeHit.instance.ShowMessage("Opps! Don't have enough apples");
			SoundManager_KnifeHit.instance.PlaybtnSfx ();
			return;
		}
		if(unlockingRandom)
		{
			return;
		}
		StartCoroutine (UnlockRandomCoKnife ());

	}
	IEnumerator UnlockRandomCoKnife()
	{
		unlockingRandom = true;
		List<ShopItem_KnifeHit> lockedItems=shopItems.FindAll((obj) => {	return !obj.KnifeUnlock; });
		ShopItem_KnifeHit randomSelect =null;
		for (int i = 0; i < lockedItems.Count *2; i++) 
		{
			randomSelect=lockedItems[Random.Range(0,lockedItems.Count)];

			if (!randomSelect.selected) {
				randomSelect.selected = true;
				SoundManager_KnifeHit.instance.PlaySingle (RandomUnlockSfx);
			}
			yield return new WaitForSeconds (.2f);
		}

		GameManager_KnifeHit.Apple -= UnlockRandomPrice;
		randomSelect.KnifeUnlock = true;
		randomSelect.UpdateUIColor ();
		GameManager_KnifeHit.SelectedKnifeIndex = randomSelect.index;
		UpdateUI ();
		unlockingRandom = false;
		SoundManager_KnifeHit.instance.PlaySingle (onUnlocksfx);

	}
}
