﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//Paul Cornel//
public class GamePlayManager_KnifeHit : MonoBehaviour {

	
	public static GamePlayManager_KnifeHit instance;
	[Header("Circle Setting")]
	public Circle_KnifeHit[] circlePrefabs;
	public Bosses[] BossPrefabs;

	public Transform circleSpawnPoint;
	[Range(0f,1f)]public float circleWidthByScreen=.5f;

	[Header("Knife Setting")]
	public Knife_KnifeHit knifePrefab;
	public Transform KnifeSpawnPoint;
	[Range(0f,1f)]public float knifeHeightByScreen=.1f;

	public GameObject ApplePrefab;
	[Header("UI Object")]
	public Text lblScore;
	public Text lblStage;
	public Text lblHighScore;
	public List<Image> stageIcons;
	public Color stageIconActiveColor;
	public Color stageIconNormalColor;

	[Header("UI Boss")]

	public GameObject bossFightStart;
	public GameObject bossFightEnd;
	public AudioClip[] bossFightStartSounds;
	public AudioClip[] bossFightEndSounds;
	[Header("Ads Show")]
	public GameObject adsShowView;
	public Image adTimerImage;
	public Text adSocreLbl;


	[Header("GameOver Popup")]
	public GameObject gameOverView;
	public Text gameOverSocreLbl,gameOverStageLbl;
	public GameObject newBestScore;
	public AudioClip gameOverSfx;
	[Space(50)]

	public int cLevel = 0;
	public bool isDebug=false;
	string currentBossName="";
	Circle_KnifeHit currentCircle;
	Knife_KnifeHit currentKnife;
	bool usedAdContinue;
	public static bool isCircleSpawn = false;
	public int totalSpawnKnife
	{
		get
		{ 
			return _totalSpawnKnife;
		}
		set
		{
			_totalSpawnKnife = value;

		}
	}
	int _totalSpawnKnife;

	void Awake()
	{	
		Application.targetFrameRate = 60;
		QualitySettings.vSyncCount = 0;
		if (instance == null) {
			instance = this;		
		}
	}
	void Start () {
		
		lblHighScore.text = GameManager_KnifeHit.HighScore + "";
		//	startGame ();
		//  CUtils.ShowInterstitialAd();
	}

	private void OnEnable()
    {
		Timer_KnifeHit.Schedule(this, 0.1f, AddEvents);
    }

    private void AddEvents()
    {
#if UNITY_ANDROID || UNITY_IOS
       /* if (AdmobController.instance.rewardBasedVideo != null)
        {
            AdmobController.instance.rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
            AdmobController.instance.rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
        }*/
#endif
    }

    bool doneWatchingAd = false;
  /*  public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        if (usedAdContinue)
        {
            doneWatchingAd = true;
            AdShowSucessfully();
        }
    }*/

    public void HandleRewardBasedVideoClosed(object sender, System.EventArgs args)
    {
        if (usedAdContinue)
        {
            if (doneWatchingAd == false)
            {
                adsShowView.SetActive(false);
                usedAdContinue = false;
                showGameOverPopup();
            }
        }
    }

    public void startGame()
	{
		GameManager_KnifeHit.score = 0;
		GameManager_KnifeHit.Stage = 1;
		GameManager_KnifeHit.isGameOver = false;
		usedAdContinue = false;
		if (isDebug) {
			GameManager_KnifeHit.Stage = cLevel;
		}
		setupGame ();
	}
	public void UpdateLable()
	{
		lblScore.text = GameManager_KnifeHit.score+"";
		lblHighScore.text = GameManager_KnifeHit.HighScore + "";
		if (GameManager_KnifeHit.Stage % 5 == 0) {
			for (int i = 0; i < stageIcons.Count-1; i++) {
				stageIcons [i].gameObject.SetActive(false);
			}
			stageIcons [stageIcons.Count-1].color = stageIconActiveColor;
			lblStage.color = stageIconActiveColor;
			lblStage.text = currentBossName;
		}
		else {
			lblStage.text = "STAGE "+ GameManager_KnifeHit.Stage;
			for (int i = 0; i < stageIcons.Count; i++) {
				stageIcons [i].gameObject.SetActive(true);
				stageIcons [i].color = GameManager_KnifeHit.Stage % stageIcons.Count <= i ? stageIconNormalColor : stageIconActiveColor;
			}
			lblStage.color = stageIconNormalColor;
		}
	}
	public void setupGame()
	{
		spawnCircle ();
		KnifeCounter_KnifeHit.intance.setUpCounter (currentCircle.totalKnife);

		totalSpawnKnife=0;
		StartCoroutine(GenerateKnife ());
	}
	void Update () {
		if (currentKnife == null)
			return;
		if (Input.GetMouseButtonDown (0) && !currentKnife.isFire) {
			KnifeCounter_KnifeHit.intance.setHitedKnife (totalSpawnKnife);
			currentKnife.ThrowKnife ();
			StartCoroutine(GenerateKnife ());
		}

	}
	public void spawnCircle()
	{
		
			GameObject tempCircle;

			if (GameManager_KnifeHit.Stage % 5 == 0)
			{
				Bosses b = BossPrefabs[Random.Range(0, BossPrefabs.Length)];
				tempCircle = Instantiate<Circle_KnifeHit>(b.BossPrefab, circleSpawnPoint.position, Quaternion.identity, circleSpawnPoint).gameObject;
				currentBossName = "Boss : " + b.Bossname;
				UpdateLable();
				OnBossFightStart();
			}
			else
			{
				if (GameManager_KnifeHit.Stage > 50)
				{
					tempCircle = Instantiate<Circle_KnifeHit>(circlePrefabs[Random.Range(11, circlePrefabs.Length - 1)], circleSpawnPoint.position, Quaternion.identity, circleSpawnPoint).gameObject;
				}
				else
				{
					tempCircle = Instantiate<Circle_KnifeHit>(circlePrefabs[GameManager_KnifeHit.Stage - 1], circleSpawnPoint.position, Quaternion.identity, circleSpawnPoint).gameObject;
				}
			}

			tempCircle.transform.localScale = Vector3.one;
			float circleScale = (GameManager_KnifeHit.ScreenWidth * circleWidthByScreen) / tempCircle.GetComponent<SpriteRenderer>().bounds.size.x;
			tempCircle.transform.localScale = Vector3.one * .2f;
			LeanTween.scale(tempCircle, new Vector3(circleScale, circleScale, circleScale), .3f).setEaseOutBounce();
			tempCircle.transform.localScale = Vector3.one * circleScale;
			currentCircle = tempCircle.GetComponent<Circle_KnifeHit>();
			currentCircle.SpawnAppleAndKnife();
			Debug.Log(" spawn circle ");
			
	}
	public IEnumerator OnBossFightStart()
	{
		bossFightStart.SetActive (true);
		SoundManager_KnifeHit.instance.PlaySingle (bossFightStartSounds[Random.Range(0,bossFightEndSounds.Length-1)],1f);
		yield return new WaitForSeconds (2f);
		bossFightStart.SetActive (false);
		setupGame ();
	}

	public IEnumerator OnBossFightEnd()
	{
		bossFightEnd.SetActive (true);
		SoundManager_KnifeHit.instance.PlaySingle (bossFightEndSounds[Random.Range(0,bossFightEndSounds.Length-1)],1f);
		yield return new WaitForSeconds (2f);
		bossFightEnd.SetActive (false);
		setupGame ();
	}
	public IEnumerator GenerateKnife()
	{
		//yield return new WaitForSeconds (0.1f);
		yield return new WaitUntil (() => {
			return KnifeSpawnPoint.childCount==0;
		});
			if (currentCircle.totalKnife > totalSpawnKnife && !GameManager_KnifeHit.isGameOver ) {
				totalSpawnKnife++;
				GameObject tempKnife;
				if (GameManager_KnifeHit.selectedKnifePrefab == null) {
					tempKnife = Instantiate<Knife_KnifeHit> (knifePrefab, new Vector3 (KnifeSpawnPoint.position.x, KnifeSpawnPoint.position.y - 2f, KnifeSpawnPoint.position.z), Quaternion.identity, KnifeSpawnPoint).gameObject;
				} else {
					tempKnife = Instantiate<Knife_KnifeHit> (GameManager_KnifeHit.selectedKnifePrefab, new Vector3 (KnifeSpawnPoint.position.x, KnifeSpawnPoint.position.y - 2f, KnifeSpawnPoint.position.z), Quaternion.identity, KnifeSpawnPoint).gameObject;
			
				}
				tempKnife.transform.localScale = Vector3.one;
				float knifeScale = (GameManager_KnifeHit.ScreenHeight * knifeHeightByScreen) / tempKnife.GetComponent<SpriteRenderer> ().bounds.size.y;
				tempKnife.transform.localScale = Vector3.one * knifeScale;
				LeanTween.moveLocalY (tempKnife, 0, 0.1f);
				tempKnife.name ="Knife"+totalSpawnKnife; 
				currentKnife = tempKnife.GetComponent<Knife_KnifeHit> ();
			}

	}
	public void NextLevel()
	{
		
		Debug.Log ("Next Level");
		if (currentCircle != null) {
			currentCircle.destroyMeAndAllKnives ();
		}
		if (GameManager_KnifeHit.Stage % 5 == 0) {
			GameManager_KnifeHit.Stage++;
			StartCoroutine (OnBossFightEnd ());

		} else {
			GameManager_KnifeHit.Stage++;
			if (GameManager_KnifeHit.Stage % 5 == 0) {
				StartCoroutine (OnBossFightStart ());
			} else {
				Invoke ("setupGame", .3f);
			}
		}
	}

	IEnumerator currentShowingAdsPopup;
	public void GameOver()
	{
		GameManager_KnifeHit.isGameOver = true;
		showGameOverPopup();
		/*if (usedAdContinue*//* || !IsAdAvailable()*//*) {
			showGameOverPopup ();
		} else {
			currentShowingAdsPopup = showAdPopup ();
			StartCoroutine (currentShowingAdsPopup);
		}*/
	}
	public IEnumerator showAdPopup()
	{
		adsShowView.SetActive (true);
		adSocreLbl.text = GameManager_KnifeHit.score+"";
		SoundManager_KnifeHit.instance.PlayTimerSound ();
		for (float i=1f; i>0; i-=0.01f) 
		{
			adTimerImage.fillAmount =i;
			yield return new WaitForSeconds (0.1f);
		}
		CancleAdsShow ();
		SoundManager_KnifeHit.instance.StopTimerSound ();
	}
	public void OnShowAds()
	{
        doneWatchingAd = false;

		SoundManager_KnifeHit.instance.StopTimerSound();
		SoundManager_KnifeHit.instance.PlaybtnSfx();
        usedAdContinue = true;
        StopCoroutine(currentShowingAdsPopup);

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
      //  AdmobController.instance.ShowRewardBasedVideo();
#else
        //HandleRewardBasedVideoRewarded(null, null);
#endif
    }
	public  void AdShowSucessfully()
    {
        adsShowView.SetActive(false);
        totalSpawnKnife--;
		GameManager_KnifeHit.isGameOver = false;
		print (currentCircle.hitedKnife.Count);
		print (totalSpawnKnife);
		KnifeCounter_KnifeHit.intance.setHitedKnife (totalSpawnKnife);
		if (KnifeSpawnPoint.childCount == 0) {		
			StartCoroutine (GenerateKnife ());
		}
	}
	public  void CancleAdsShow()
	{
		SoundManager_KnifeHit.instance.StopTimerSound ();
		SoundManager_KnifeHit.instance.PlaybtnSfx ();
		StopCoroutine (currentShowingAdsPopup);
		adsShowView.SetActive (false);
		showGameOverPopup ();
	}
	public void showGameOverPopup()
	{
		gameOverView.SetActive (true);
		gameOverSocreLbl.text = GameManager_KnifeHit.score+"";
		gameOverStageLbl.text = "Stage "+ GameManager_KnifeHit.Stage;

		if (GameManager_KnifeHit.score >= GameManager_KnifeHit.HighScore) {
			GameManager_KnifeHit.HighScore = GameManager_KnifeHit.score;
			newBestScore.SetActive (true);
		} else {
			newBestScore.SetActive (false);
		}

		CUtils_KnifeHit.ShowInterstitialAd();
	}
	public void OpenShop()
	{
		SoundManager_KnifeHit.instance.PlaybtnSfx ();
		KnifeShop_KnifeHit.intance.showShop ();
	}
	public void RestartGame()
	{
		SoundManager_KnifeHit.instance.PlaybtnSfx ();
		GeneralFunction_KnifeHit.intance.LoadSceneByName ("Game");
	}
	public void BackToHome()
	{
		SoundManager_KnifeHit.instance.PlaybtnSfx ();
		GeneralFunction_KnifeHit.intance.LoadSceneByName ("Home");
	}
	public void FBClick()
	{
		SoundManager_KnifeHit.instance.PlaybtnSfx ();
        StartCoroutine(CROneStepSharing());
    }
	public void ShareClick()
	{
		SoundManager_KnifeHit.instance.PlaybtnSfx ();
        StartCoroutine(CROneStepSharing());
	}
	public void SettingClick()
	{
		SoundManager_KnifeHit.instance.PlaybtnSfx ();
		SettingUI_KnifeHit.intance.showUI ();
	}

    IEnumerator CROneStepSharing()
    {
        yield return new WaitForEndOfFrame();
      //  MobileNativeShare.ShareScreenshot("screenshot", "");
    }
}

[System.Serializable]
public class Bosses{
	public string Bossname;
	public Circle_KnifeHit BossPrefab;
}
