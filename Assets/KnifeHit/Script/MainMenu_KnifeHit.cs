﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
//Paul Cornel//
public class MainMenu_KnifeHit : MonoBehaviour 
{
	[Header("Main View")]
	public Button giftButton;
	public Text giftLable;
	public CanvasGroup giftLableCanvasGroup;
	public GameObject giftBlackScreen;
	public GameObject giftParticle;
	public Image selectedKnifeImage;
	public AudioClip giftSfx;

	public static MainMenu_KnifeHit intance;

	// Gift Setting

	int timeForNextGift = 60*8;
	int minGiftApple = 40;// Minimum Apple for Gift
	int maxGiftApple = 70;// Maxmum Apple for Gift
	void Awake()
	{
		intance = this;
	}
	void Start()
	{
         /* CUtils.ShowInterstitialAd();
        	InvokeRepeating ("updateGiftStatus", 0f, 1f);
        	KnifeShop_KnifeHit.intance.UpdateUI (); */
    }

    public void OnPlayClick()
	{
		//SoundManager.instance.PlaybtnSfx ();
		//GeneralFunction_KnifeHit.intance.LoadSceneWithLoadingScreen ("Game");
		GamePlayManager_KnifeHit.instance.startGame();
		gameObject.SetActive(false);
	}
	public void RateGame()
	{
		SoundManager_KnifeHit.instance.PlaybtnSfx ();
		CUtils_KnifeHit.OpenStore();
	}

	void updateGiftStatus()
	{
		if (GameManager_KnifeHit.GiftAvalible) {
			giftButton.interactable = true;
			LeanTween.alphaCanvas (giftLableCanvasGroup, 0f, .4f).setOnComplete (() => {
				LeanTween.alphaCanvas (giftLableCanvasGroup, 1f, .4f);
			});
			giftLable.text="READY!";
		} else {
			giftButton.interactable = false;
			giftLable.text = GameManager_KnifeHit.RemendingTimeSpanForGift.Hours.ToString("00")+":"+
				GameManager_KnifeHit.RemendingTimeSpanForGift.Minutes.ToString("00")+":"+
				GameManager_KnifeHit.RemendingTimeSpanForGift.Seconds.ToString("00");
		}
	}
	[ContextMenu("Get Gift")]
	public void OnGiftClick()
	{
		SoundManager_KnifeHit.instance.PlaybtnSfx ();
		int Gift = UnityEngine.Random.Range (minGiftApple, maxGiftApple);
		Toast_KnifeHit.instance.ShowMessage("You got "+Gift+" Apples");
		GameManager_KnifeHit.Apple += Gift;
		GameManager_KnifeHit.NextGiftTime = DateTime.Now.AddMinutes(timeForNextGift);

        updateGiftStatus ();
		giftBlackScreen.SetActive (true);
		Instantiate<GameObject>(giftParticle);
		SoundManager_KnifeHit.instance.PlaySingle (giftSfx);
		Invoke("HideGiftParticle",2f);
	}
	public void HideGiftParticle()
	{
		giftBlackScreen.SetActive (false);
	}
	public void OpenShopUI()
	{
		SoundManager_KnifeHit.instance.PlaybtnSfx ();
		KnifeShop_KnifeHit.intance.showShop ();	
	}
	public void OpenSettingUI()
	{
		SoundManager_KnifeHit.instance.PlaybtnSfx ();
		SettingUI_KnifeHit.intance.showUI();	
	}
}

